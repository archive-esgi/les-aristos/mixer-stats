run:
	docker-compose up -d

run-with-logs:
	docker-compose up

stop:
	docker-compose down

build:
	docker-compose build

run-build:
	docker-compose up --build

linter_phpcs:
	docker run -ti --rm -v ${CURDIR}/src:/app sixlive/php-lint-fix php-cs-fixer fix --dry-run --diff .

test_unit:
	bin/phpunit tests/unit

test_integration:
	bin/phpunit tests/integration

test_functional:
	docker run -it --rm -v ${CURDIR}:/usr/src/myapp -w /usr/src/myapp php:7.3-cli php vendor/bin/behat
