<?php

namespace App\Service\Math;

/**
 * Class MultiplyService
 * @package App\Service\Math
 */
class MultiplyService
{
    public function __construct()
    {
    }

    /**
     * Return the result of the multiplication operation
     *
     * @param float $number1
     * @param float $number2
     * @return float
     */
    public function calc(float $number1, float $number2): float
    {
        return $number1 * $number2;
    }
}
