<?php

namespace App\Tests\Unit;

use App\Service\Math\ModuloService;
use App\Service\Math\SumService;
use PHPUnit\Framework\TestCase;

class SumServiceTest extends TestCase
{
    public function testCalc()
    {
        $firstNum = 5;
        $secondNum = 2;
        $service = new SumService();
        $result = $service->calc($firstNum, $secondNum);

        $this->assertSame(7.0, $result);
    }
}
