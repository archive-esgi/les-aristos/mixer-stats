<?php

namespace App\Tests\Unit;

use App\Service\Math\MultiplyService;
use PHPUnit\Framework\TestCase;

class MultiplyServiceTest extends TestCase
{
    public function testCalc()
    {
        $firstNum = 4;
        $secondNum = 2;
        $service = new MultiplyService();
        $result = $service->calc($firstNum, $secondNum);

        $this->assertSame(8.0, $result);
    }
}
