---
name: "Bug report"
about: Create a report to help us improve
---

<!--
Thank you for reporting a possible bug.

Please fill in as much of the template below as you can.
-->

## Informations

<!--
    Version: version of our application that you use
    Platform: output of `uname -a` (UNIX), or version and 32 or 64-bit (Windows)
    Subsystem: if known, please specify the affected core module name
-->

*  **Version used**: 

*  **Your platform**:

*  **Your subsystem**:

## Details of the report

<!--
    If possible, please provide code that demonstrates the problem, keeping it as
simple and free of external dependencies as you can.
-->

*  **Description of the bug**:
<!-- Please provide more details here -->

*  **How to reproduce it**:
<!--
    If possible, please provide code that demonstrates the problem, keeping it as
simple and free of external dependencies as you can.
-->


